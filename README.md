# MP3 Player

A Pen created on CodePen.io. 
Audio player for playing music and audio made by CSS and a library on JavaScript called waveSurfer.
This player is made especially for my next Spotify like project so I hope you support me a little until I finish it.
Have a nice day.
[DEMO](https://readloud.github.io/mp3-player/dist)

Original URL: [codepen.io@elattar-ayoub](https://codepen.io/elattar-ayoub/pen/jOLGKpQ).